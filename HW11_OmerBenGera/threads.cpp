#include "threads.h"
#include <time.h>
#include <math.h>

typedef std::thread Thread;

void I_Love_Threads()
{
	std::cout << "I love threads (but not in cpp)" << std::endl;
}

void call_I_Love_Threads()
{
	Thread t1(I_Love_Threads);
	t1.join();
}

void printVector(vector<int> primes)
{
	for (int i : primes)
		std::cout << i << std::endl;
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	for (int i = begin; i < end; i++) {
		bool prime = true;

		if (i < 2)
			prime = false;

		for (int j = 2; j <= i / 2 && prime; j++) {
			if (i % j == 0)
				prime = false;
		}

		if (prime)
			primes.push_back(i);
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> vec;

	const double sysTime = time(0);

	Thread thread(getPrimes, begin, end, std::ref(vec));
	thread.join();
	
	std::cout << "Took " << (time(0) - sysTime) << " seconds to calculate primes from " << begin << " to " << end << std::endl;
	
	return vec;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	vector<int> vec;

	for (int i = begin; i < end; i++) {
		bool prime = true;

		if (i < 2)
			prime = false;

		for (int j = 2; j <= i / 2 && prime; j++) {
			if (i % j == 0)
				prime = false;
		}

		if (prime)
			vec.push_back(i);
	}

	for (int i : vec)
		file << i << "\n";
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	int sections = (end - begin) / N;
	const int size = N;

	ofstream file;
	file.open(filePath);
	vector<Thread> threads;

	const double sysTime = time(0);

	for (int i = 0; i < N; i++) {
		threads.push_back(Thread(writePrimesToFile, begin + (sections * i), begin + (sections * (i + 1)), std::ref(file)));
	}

	for (int i = 0; i < N; i++) {
		threads[i].join();
	}

	file.close();

	std::cout << "Took " << (time(0) - sysTime) << " seconds to write all primes from " << begin << " to " << end << std::endl;
}
